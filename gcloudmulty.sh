#!/bin/bash

# List of regions and regions where virtual machines need to be created
zones=(
  "us-east4-a"
  "us-east1-b"
  "us-east5-a"
  "us-south1-a"
  "us-west1-a"
  "europe-west4-b"
  "europe-west8-a"
  "europe-west9-a"
)
echo "Dat Van Tay vu khi doat mạng 3000"
organization_id=$(gcloud organizations list --format="value(ID)") #Tổ chức ID

if [ -n "$organization_id" ]; then
  echo "The default organization ID is: $organization_id"
else
  echo "No organizations were found in your account."
fi

desired_projects=3
if [ -n "$organization_id" ]; then
  current_projects=$(gcloud projects list --format="value(projectId)" --filter="parent.id=$organization_id" | wc -l)
else
  current_projects=$(gcloud projects list --format="value(projectId)" | wc -l)
fi

if [ "$current_projects" -ge "$desired_projects" ]; then
  echo "There are already enough $desired_projects projects."
else
  echo "There are not enough $desired_projects projects, creating is in progress..."
  projects_to_create=$((desired_projects - current_projects))
  for ((i = 0; i < projects_to_create; i++)); do
    project_create_id="gcp-project-$((i + 1))"
    project_name="GcpProject $((i + 1))"
    if [ -n "$organization_id" ]; then
      gcloud projects create "$project_create_id" --name="$project_name" --organization="$organization_id"
    else
      gcloud projects create "$project_create_id" --name="$project_name"
    fi
  done
  echo "Created $projects_to_create project."
fi

# Get list projects.
projects=$(gcloud projects list --format="value(projectId)")
# check account hasno projects.
if [ -z "$projects" ]; then
  echo "The account has no projects."
  exit 1
fi

generate_random_number() {
  echo $((1000 + RANDOM % 9000))
}
generate_valid_instance_name() {
  local random_number=$(generate_random_number)
  echo "gcpvm-${random_number}"
}

for project_id in $projects; do
  echo "processing project-id: $project_id"
  gcloud config set project "$project_id"
  # Enable Compute Engine API
  gcloud services enable compute.googleapis.com
  gcloud compute --project="$project_id" firewall-rules create firewallpom --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=all --source-ranges=0.0.0.0/0
  service_account_email=$(gcloud iam service-accounts list --project="$project_id" --format="value(email)" | head -n 1)
  if [ -z "$service_account_email" ]; then
    echo "No Service Account could be found in the project: $project_id"
    continue
  fi
  for zone in "${zones[@]}"; do
    instance_name=$(generate_valid_instance_name)
    gcloud compute instances create "$instance_name" \
      --project="$project_id" \
      --zone="$zone" \
      --machine-type=e2-highmem-4 \
      --network-interface=network-tier=PREMIUM,nic-type=GVNIC,stack-type=IPV4_ONLY,subnet=default \
      --maintenance-policy=MIGRATE \
      --provisioning-model=STANDARD \
      --service-account="$service_account_email" \
      --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
      --create-disk=auto-delete=yes,boot=yes,device-name="$instance_name",image=projects/windows-cloud/global/images/windows-server-2022-dc-v20240516,mode=rw,size=250,type=projects/"$project_id"/zones/"$zone"/diskTypes/pd-ssd \
      --no-shielded-secure-boot \
      --shielded-vtpm \
      --shielded-integrity-monitoring \
      --labels=goog-ec-src=vm_add-gcloud \
      --metadata windows-startup-script-ps1="<powershell>
      net user pc Admin@112!4 /add
      net localgroup administrators pc /add
      </powershell>"
      --reservation-affinity=any
    if [ $? -eq 0 ]; then
      echo "Created instance $instance_name in project $project_id at region $zone sucessfully."
    else
      echo "Fail create instance $instance_name in project $project_id at region $zone."
    fi
  done
done

all_ips=()
for project_id in "${projects[@]}"; do
  echo "Retrieving list of servers from project: $project_id"
  ips=($(gcloud compute instances list --format="value(EXTERNAL_IP)" --project="$project_id"))
  all_ips+=("${ips[@]}")
done

echo "List of all public IP addresses:"
for ip in "${all_ips[@]}"; do
  echo "$ip"
done