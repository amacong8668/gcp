#!/bin/bash
startup_script_url="https://raw.githubusercontent.com/gcpmore8668/gcpnode/main/inittitan.sh"
# List of regions and regions where virtual machines need to be created
zones=(
  "us-east4-a"
  "us-east1-b"
  "us-east5-a"
  "us-south1-a"
  "us-west1-a"
  "europe-west4-b"
  "europe-west8-a"
  "europe-west9-a"
)
echo "Dat Van Tay vu khi doat mạng 3000"
generate_random_project_id() {
  echo "my-project-$(tr -dc 'a-z0-9' < /dev/urandom | head -c 8)"
}

# Lấy organization ID nếu có
organization_id=$(gcloud organizations list --format="value(ID)")

if [ -n "$organization_id" ]; then
  echo "Tổ chức mặc định có ID là: $organization_id"
else
  echo "Không tìm thấy tổ chức nào trong tài khoản của bạn."
fi

billing_account_id=$(gcloud beta billing accounts list --format="value(ACCOUNT_ID)" | head -n 1)

# Số lượng dự án cần tạo
desired_projects=3

# Lấy số lượng dự án hiện tại trong tổ chức (nếu có tổ chức)
if [ -n "$organization_id" ]; then
  current_projects=$(gcloud projects list --format="value(projectId)" --filter="parent.id=$organization_id" | wc -l)
else
  current_projects=$(gcloud projects list --format="value(projectId)" | wc -l)
fi

if [ "$current_projects" -ge "$desired_projects" ]; then
  echo "Đã có đủ $desired_projects dự án trong tổ chức có ID $organization_id."
else
  echo "Chưa có đủ $desired_projects dự án trong tổ chức có ID $organization_id, đang tiến hành tạo..."

  projects_to_create=$((desired_projects - current_projects))

  for ((i = 0; i < projects_to_create; i++)); do
    while true; do
      projectcr_id=$(generate_random_project_id)
      project_name="My Project $((current_projects + i + 1))"

      # Tạo dự án mới và gán vào tổ chức nếu có
      if [ -n "$organization_id" ]; then
        gcloud projects create "$projectcr_id" --name="$project_name" --organization="$organization_id"
      else
        gcloud projects create "$projectcr_id" --name="$project_name"
      fi

      if [ $? -eq 0 ]; then
        break
      else
        echo "Project ID $projectcr_id đã được sử dụng, thử lại với ID khác."
      fi
    done
  done

  echo "Đã tạo và kích hoạt API cho $projects_to_create dự án."
fi

# Lấy danh sách tất cả các dự án hiện tại
if [ -n "$organization_id" ]; then
  project_list=$(gcloud projects list --format="value(projectId)" --filter="parent.id=$organization_id")
else
  project_list=$(gcloud projects list --format="value(projectId)")
fi

# Get list projects.
projects=$(gcloud projects list --format="value(projectId)")
# check account hasno projects.
if [ -z "$projects" ]; then
  echo "The account has no projects."
  exit 1
fi

# Kích hoạt API Compute Engine cho tất cả các dự án
for project_ide in $project_list; do
  gcloud services enable compute.googleapis.com --project "$project_ide"
done

generate_random_number() {
  echo $((1000 + RANDOM % 9000))
}
generate_valid_instance_name() {
  local random_number=$(generate_random_number)
  echo "gcpvm-${random_number}"
}

for project_id in $projects; do
  echo "processing project-id: $project_id"
  gcloud config set project "$project_id"
  gcloud compute --project="$project_id" firewall-rules create firewalldev --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=all --source-ranges=0.0.0.0/0
  service_account_email=$(gcloud iam service-accounts list --project="$project_id" --format="value(email)" | head -n 1)
  if [ -z "$service_account_email" ]; then
    echo "No Service Account could be found in the project: $project_id"
    continue
  fi
  for zone in "${zones[@]}"; do
    instance_name=$(generate_valid_instance_name)
     gcloud compute instances create "$instance_name" \
    --project="$project_id" \
    --zone="$zone" \
    --machine-type=t2d-standard-1 \
    --network-interface=network-tier=PREMIUM,nic-type=GVNIC,stack-type=IPV4_ONLY,subnet=default \
    --maintenance-policy=MIGRATE \
    --provisioning-model=STANDARD \
    --service-account="$service_account_email" \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --create-disk=auto-delete=yes,boot=yes,device-name="$instance_name",image=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20240607,mode=rw,size=139,type=projects/"$project_id"/zones/"$zone"/diskTypes/pd-balanced \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --labels=goog-ec-src=vm_add-gcloud \
    --metadata=startup-script-url="$startup_script_url" \
    --reservation-affinity=any
    if [ $? -eq 0 ]; then
      echo "Created instance $instance_name in project $project_id at region $zone sucessfully."
    else
      echo "Fail create instance $instance_name in project $project_id at region $zone."
    fi
  done
done

all_ips=()
for projectip_id in "${projects[@]}"; do
  echo "Retrieving list of servers from project: $projectip_id"
  ips=($(gcloud compute instances list --format="value(EXTERNAL_IP)" --project="$projectip_id"))
  all_ips+=("${ips[@]}")
done

echo "List of all public IP addresses:"
for ip in "${all_ips[@]}"; do
  echo "$ip"
done