#!/bin/bash
startup_script_url="https://raw.githubusercontent.com/gcpmore8668/gcpnode/main/inittitan.sh"
# List of regions and regions where virtual machines need to be created
zones=(
  "us-east4-a"
  "us-east1-b"
  "us-east5-a"
  "us-south1-a"
  "us-west1-a"
  "europe-west4-b"
  "europe-west8-a"
  "europe-west9-a"
)
echo "Dat Van Tay vu khi doat mạng 3000"

# Get list projects.
projects=$(gcloud projects list --format="value(projectId)")
echo "projects: $projects ....."
# check account hasno projects.
if [ -z "$projects" ]; then
  echo "The account has no projects."
  exit 1
fi

# Kích hoạt API Compute Engine cho tất cả các dự án
for project_ide in $projects; do
  echo "enable compute.googleapis.com project: $project_ide ....."
  gcloud services enable compute.googleapis.com --project "$project_ide"
  sleep 3
  echo "enabled compute.googleapis.com project: $project_ide"
done

generate_random_number() {
  echo $((1000 + RANDOM % 9000))
}
generate_valid_instance_name() {
  local random_number=$(generate_random_number)
  echo "gcpnode-${random_number}"
}

for project_id in $projects; do
  echo "processing project-id: $project_id"
  gcloud config set project "$project_id"
  gcloud compute --project="$project_id" firewall-rules create firewalldev --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=all --source-ranges=0.0.0.0/0
  service_account_email=$(gcloud iam service-accounts list --project="$project_id" --format="value(email)" | head -n 1)
  if [ -z "$service_account_email" ]; then
    echo "No Service Account could be found in the project: $project_id"
    continue
  fi
  for zone in "${zones[@]}"; do
    instance_name=$(generate_valid_instance_name)
     gcloud compute instances create "$instance_name" \
    --project="$project_id" \
    --zone="$zone" \
    --machine-type=t2d-standard-1 \
    --network-interface=network-tier=PREMIUM,nic-type=GVNIC,stack-type=IPV4_ONLY,subnet=default \
    --maintenance-policy=MIGRATE \
    --provisioning-model=STANDARD \
    --service-account="$service_account_email" \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --create-disk=auto-delete=yes,boot=yes,device-name="$instance_name",image=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20240607,mode=rw,size=139,type=projects/"$project_id"/zones/"$zone"/diskTypes/pd-balanced \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --labels=goog-ec-src=vm_add-gcloud \
    --metadata=startup-script-url="$startup_script_url" \
    --reservation-affinity=any
    if [ $? -eq 0 ]; then
      echo "Created instance $instance_name in project $project_id at region $zone sucessfully."
    else
      echo "Fail create instance $instance_name in project $project_id at region $zone."
    fi
  done
done

projectsss=($(gcloud projects list --format="value(projectId)"))

# Mảng chứa tất cả các địa chỉ IP công cộng
all_ips=()

# Lặp qua từng dự án và lấy danh sách các địa chỉ IP công cộng
for projects_id in "${projectsss[@]}"; do
  echo "Retrieving list of servers from project: $projects_id"
  
  # Đặt dự án hiện tại
  gcloud config set project "$projects_id"
  
  # Lấy danh sách địa chỉ IP công cộng của các máy chủ trong dự án hiện tại
  ips=($(gcloud compute instances list --format="value(EXTERNAL_IP)" --project="$projects_id"))
  
  # Thêm các địa chỉ IP vào mảng all_ips
  all_ips+=("${ips[@]}")
done

# In ra danh sách tất cả các địa chỉ IP công cộng
echo "List of all public IP addresses:"
for ip in "${all_ips[@]}"; do
  echo "$ip"
done