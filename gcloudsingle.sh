#!/bin/bash
# Function to generate a random 4-digit number
generate_random_number() {
  echo $((1000 + RANDOM % 9000))
}
# Function to generate a valid instance name starting with "staging" and followed by a random 4-digit number
generate_valid_instance_name() {
  local random_number=$(generate_random_number)
  echo "staging-${random_number}"
}
# Danh sách các vùng và khu vực cần tạo máy ảo
zones=(
  "us-east4-a"
  "us-east1-b"
  "us-east5-a"
  "us-south1-a"
  "us-west1-a"
  "europe-west4-b"
  "europe-west8-a"
  "europe-west9-a"
)

project_id=$(gcloud projects list --format="value(projectId)" | head -n 1)
gcloud config set project "$project_id"
gcloud services enable compute.googleapis.com
service_id=$(gcloud iam service-accounts list --project="$project_id" --format="value(email)" | head -n 1)
startup_script_url="https://raw.githubusercontent.com/gcpmore8668/gcpnode/main/inittitan.sh"

gcloud compute --project="$project_id" firewall-rules create firewallgcp --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=all --source-ranges=0.0.0.0/0

for zone in "${zones[@]}"; do
  instance_name=$(generate_valid_instance_name)
  gcloud compute instances create "$instance_name" \
    --project="$project_id" \
    --zone="$zone" \
    --machine-type=t2d-standard-1 \
    --network-interface=network-tier=PREMIUM,nic-type=GVNIC,stack-type=IPV4_ONLY,subnet=default \
    --maintenance-policy=MIGRATE \
    --provisioning-model=STANDARD \
    --service-account="$service_id" \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --create-disk=auto-delete=yes,boot=yes,device-name="$instance_name",image=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20240607,mode=rw,size=139,type=projects/"$project_id"/zones/"$zone"/diskTypes/pd-balanced \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --labels=goog-ec-src=vm_add-gcloud \
    --metadata=startup-script-url="$startup_script_url" \
    --reservation-affinity=any
done